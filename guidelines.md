General Rules/Guidelines to Follow for New Candidates:

- Candidate should have a sense of humor. Gaming can be serious, but you should remember to have fun.
- No Douche Bags, Trolls, or  Hackers allowed.
- Must be able to control rage.
- Don't scold your teammates, unless you're being sarcastic. If you lose a game due to another teammates inexperience, help them. Provide constructive input. Teach them as if they were a good friend, which we all hope to become.
- Candidate should demonstrate adequate skills. They don't necessarily need to be diamond/masters all ranks, just demonstrate the capability they could reach that stage one day or learn to.
- Racism will not be tolerated. You can joke about racism, but seriously don't be a racist tool.
- General conduct can be described here: http://eu.battle.net/en/community/conduct
- Must have a good network connection, sorry we generally don't appreciate lag.

Just a few good gestures regarding gameplay:

- Don't leave a teammate hanging. If you're in a full party, and see a teammate hanging solo, unless there intentions are to 1v1, try to include them somehow. Leave your party to join them.