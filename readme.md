

Project Titan is an opensource project for managing our development efforts for the Starcraft 2 game engine. All scripts are to reference the Apache 2.0 license:

http://www.apache.org/licenses/LICENSE-2.0.html